# SC19 Tutorial

Basic Kubernetes\
Hands on session

## Setup

There are several basic step you must take to

1. Install the kubectl Kubernetes client.\
   Instructions at <https://kubernetes.io/docs/tasks/tools/install-kubectl/>\
   I recommend just downloading the static binary (the curl way)

2. Get your configuration file for the PRP/Nautilus Kubernetes cluster from organizers, and put it as \~/.kube/config.

The config files for this tutorial will have the right namespace pre-set. In general you need to be aware of which namespace you’re working in, and either set it with `kubectl config set-context nautilus --namespace=the_namespace` or specify in each `kubbectl` command by adding `-n namespace`.

## Explore the system

To get the list of cluster nodes (although you may not have access to all of them), type:

```
kubectl get nodes
```

Right now you don't have anything running in the namespace, and these commands will return `No resources found in sc19-demo namespace.`, but later these will be useful to see what's running:

List all the pods in your namespace

```
kubectl get pods
```

List all the deployments in your namespace

```
kubectl get deployments
```

List all the services in your namespace

```
kubectl get services
```

## Launch a simple pod

Let’s create a simple generic pod, and login into it.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
All the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-username
spec:
  containers:
  - name: mypod
    image: centos:centos7
    resources:
      limits:
        memory: 100Mi
        cpu: 100m
      requests:
        memory: 100Mi
        cpu: 100m
    command: ["sh", "-c", "sleep infinity"]
```

Reminder, indentation is important in YAML, just like in Python.

*If you don't want to create the file and are using mac or linux, you can create yaml's dynamically like this:*

```bash
kubectl create -f - << EOF
<contents you want to deploy>
EOF
```

Now let’s start the pod:

```
kubectl create -f pod1.yaml
```

See if you can find it:

```
kubectl get pods
```

Note: You may see the pods from the other participants, too.

If it is not yet in Running state, you can check what is going on with

```
kubectl get events --sort-by=.metadata.creationTimestamp
```

Then let’s log into it

```
kubectl exec -it pod-username -- /bin/bash
```

You are now inside the (container in the) pod!

Does it feel any different than a regular, dedicated node?

Try to create some directories and some files with content.

(Hello world will do, but feel free to be creative)

We will want to check the status of the networking.

But ifconfig is not available in the image we are using; so let’s install it

```
yum install net-tools
```

Now check the the networking:

```
ifconfig -a
```

Get out of the Pod (with either Control-D or exit).

You should see the same IP displayed with kubectl

```
kubectl get pod -o wide pod-username
```

We can now destroy the pod

```
kubectl delete -f pod1.yaml
```

Check that it is actually gone:

```
kubectl get pods
```

Now, let’s create it again:

```
kubectl create -f pod1.yaml
```

Does it have the same IP?

```
kubectl get pod -o wide pod-username
```

Log back into the pod:

```
kubectl exec -it pod-username -- /bin/bash
```

What does the network look like now?

What is the status of the files your created?

Finally, let’s delete explicitly the pod:

```
kubectl delete pod pod-username
```

## Let’s make it a deployment

You saw that when a pod was terminated, it was gone.

While above we did it by ourselves, the result would have been the same if a node died or was restarted.

In order to gain a higher availability, the use of Deployments is recommended. So, that’s what we will do next.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
As mentioned before, all the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### dep1.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dep-username
  labels:
    k8s-app: dep-username
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: dep-username
  template:
    metadata: 
      labels:
        k8s-app: dep-username
    spec:
      containers:
      - name: mypod
        image: centos:centos7
        resources:
           limits:
             memory: 1.5Gi
             cpu: 1
           requests:
             memory: 0.5Gi
             cpu: 0.1
        command: ["sh", "-c", "sleep infinity"]
```

Now let’s start the deployment:

```
kubectl create -f dep1.yaml
```

See if you can find it:

```
kubectl get deployments
```

Note: You may see the deployments from the other participants, too.

The Deployment is just a conceptual service, though.

See if you can find the associated pod:

```
kubectl get pods
```

Once you have found its name, let’s log into it

```
kubectl get pod -o wide dep-username-hash
kubectl exec -it dep-username-hash -- /bin/bash
```

You are now inside the (container in the) pod!

Create directories and files as before.

Try various commands as before.

Let’s now delete the pod!

```
kubectl delete pod dep-username-hash
```

Is it really gone?

```
kubectl get pods 
```

What happened to the deployment?

```
kubectl get deployments
```

Get into the new pod

```
kubectl get pod -n sc19-demo -o wide dep-username-hash
kubectl exec -it dep-username-hash -n sc19-demo -- /bin/bash
```

Was anything preserved?

Let’s now delete the deployment:

```
kubectl delete -f dep1.yaml
```

Verify everything is gone:

```
kubectl get deployments
kubectl get pods
```

## The end
