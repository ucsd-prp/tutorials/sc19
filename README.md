# SC19 Tutorial: Unified Cyber Infrastructure with Kubernetes

### Igor Sfiligoi, Dmitry Mishin

Kubernetes has emerged as the leading container orchestration solution over the past few years. Developed at Google, now maintained by Cloud Native Foundation, it sports a very diverse and active development community. While initially developed for the cloud, it can also be deployed on-prem. It also supports federating several independent clusters into much larger logical resource pools, which can span multiple administrative and geographical regions.

One of Kubernetes advantages is its ability to effectively and securely co-schedule service containers alongside user containers. Service applications run with high privileges, with functionality and performance typical of bare metal deployments. User applications are given only basic privileges and limited resources, allowing for both a safe operating environment and fair resource sharing. The Kubernetes framework provides the glue to link them together allowing for a fully functional system.

In this tutorial, the attendees will learn how Kubernetes was used to create a nationwide cyber infrastructure that is serving hundreds of scientific groups through NFS-funded projects like OSG, PRP, TNRP and CHASE-CI. The program includes a Kubernetes architectural overview, hands on sessions operating on the PRP production Kubernetes cluster, and examples of how OSG and PRP are leveraging the Kubernetes cluster for facilitating scientific computing.

https://sc19.supercomputing.org/presentation/?id=tut130&sess=sess190

RocketChat channel: https://rocket.nautilus.optiputer.net/channel/sc19-tutorial
